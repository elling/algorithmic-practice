import random
import sys

LETTERS = 'abcdefghijklmnopqrstuvwxyz'

def random_encoding():
    tmp = list(LETTERS)
    random.shuffle(tmp)
    return {a:b for a,b in zip(LETTERS, tmp)}

def process(word):
    return ''.join(c for c in word.lower() if c in LETTERS)# or c in '\'')

def encode(cipher, word):
    return ''.join(cipher[c] if c in cipher else c for c in word)

wordset = set()

cipher = random_encoding()

text = []
for line in sys.stdin:
    words = [process(word) for word in line.split()]
    words = [word.strip() for word in words if word.strip()]
    for word in words:
        if word.strip():
            wordset.add(word)

    text.append(' '.join(encode(cipher, word) for word in words))


wordlist = list(wordset)
wordlist.sort()

print(len(wordlist), len(text))
for word in wordlist:
    print(word)
for line in text:
    print(line)
