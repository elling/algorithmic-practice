import sys
from collections import defaultdict

def read_input():
    M, N = [int(x) for x in input().split()]

    words = []
    for _ in range(M):
        words.append(input().strip())

    text = []
    for _ in range(N):
        text.append(input().strip())

    return words, text

# 1. Create a list of codewords (words seen in the encoded text)
def extract_codewords(text):
    words = {word for line in text for word in line.split()}
    return sorted(list(words))

def covered(word, mapping):
    return all(c in mapping for c in word)

def translate(word, mapping):
    return ''.join(mapping.get(c, '?') for c in word)

def group_by_size(words):
    by_size = defaultdict(list)
    for word in words:
        by_size[len(word)].append(word)
    return [x[2] for x in sorted((len(g), len(g[0]), g) for g in by_size.values())]

class State:
    def __init__(self, words, codewords, map_fwd, map_rev):
        self.words = list(words)
        self.map_fwd = map_fwd.copy()
        self.map_rev = map_rev.copy()

        self.failed = False
        self.codewords = []
        for codeword in codewords:
            if not covered(codeword, map_rev):
                self.codewords.append(codeword)
            elif translate(codeword, map_rev) not in words:
                self.failed = True
                return

        self.words_by_size = defaultdict(list)
        for word in self.words:
            self.words_by_size[len(word)].append(word)

        self.codewords_by_size = defaultdict(list)
        for word in self.codewords:
            self.codewords_by_size[len(word)].append(word)

        if self.codewords:
            self.smallest_group = min(self.codewords_by_size.values(), key=lambda x: len(x))

    def clone(self):
        return State(self.words, self.codewords, self.map_fwd, self.map_rev)

    def clone_with(self, word, codeword):
        cw2 = [cw for cw in self.codewords if cw != codeword]
        clone = State(self.words, cw2, self.map_fwd, self.map_rev)
        clone = self.clone()

        if clone.failed:
            return None

        for a,b in zip(word, codeword):
            clone.map_fwd[a] = b
            clone.map_rev[b] = a

        return clone

    def __str__(self):
        out = ""
        out += "map->: " + str(self.map_fwd) + "\n"
        out += "map<-: " + str(self.map_rev) + "\n"
        out += "words: " + str(self.words) + "\n"
        out += "codewords: " + str(self.codewords) + "\n"
        if self.failed:
            out += "*failed*\n"
        else:
            out += "smallest: " + str(self.smallest_group) + "\n"
        return out

    def test_assign(self, word, codeword):
        for a,b in zip(word, codeword):
            if self.map_rev.get(b, a) != a:
                return False
        return True

    def search(self, depth=0):
        if self.failed:
            return None

        if not self.codewords:
            return self.map_fwd, self.map_rev

        codeword = self.smallest_group[0]
        for word in self.words_by_size[len(codeword)]:
            print(depth*'  ', f'trying {codeword} -> {word}')
            if self.test_assign(word, codeword):
                child = self.clone_with(word, codeword)
                if child is None:
                    continue

                solution = child.search(depth=depth+1)
                if solution: return solution
        return None



words, text = read_input()
codewords = extract_codewords(text)

state = State(words, codewords, {}, {})
print(state)

mf, mr = state.search()
for line in text:
    print(''.join(mr.get(c, c) for c in line))
