import sys
import resource

import generate

class BitSet:
    def __init__(self, bias=0, size=1024):
        size = (size // 8) + (1 if size % 8 else 0)
        self.bias = bias
        self.data = bytearray(size * [0])

    def add(self, n):
        m = n - self.bias
        assert 0 <= m < 8 * len(self.data)
        mbyte = m // 8
        mbit = m % 8
        self.data[mbyte] = self.data[mbyte] | (1 << mbit)

    def has(self, n):
        m = n - self.bias
        assert 0 <= m < 8 * len(self.data)
        mbyte = m // 8
        mbit = m % 8
        return self.data[mbyte] & (1 << mbit)

bitset = BitSet(bias=generate.PHONE_MIN, size=(generate.PHONE_MAX+1 - generate.PHONE_MIN))
for line in sys.stdin:
    bitset.add(int(line))

for n in range(generate.PHONE_MIN, generate.PHONE_MAX+1):
    if bitset.has(n):
        print(n)
