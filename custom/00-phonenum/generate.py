import random
import sys

PHONE_MIN = 2000000
PHONE_MAX = 9999999

if __name__ == "__main__":
    count = int(sys.argv[1])

    numbers = set()

    while len(numbers) < count:
        numbers.add(random.randint(PHONE_MIN, PHONE_MAX))

    numbers = list(numbers)
    random.shuffle(numbers)

    for num in numbers:
        print(num)

