# Algorithmic Practice

A repository for small practice problems, possibly competition-style programming,
and related experiments.

## Contents

* [notes/algorithms](notes/algorithms.md) - Useful algorithms
* [notes/datastructures](notes/datastructures.md) - Useful data structures
* [euler](euler/) - Project euler problems

I have an existing [repo](https://gitlab.com/elling/aoc) of AoC problems, or
else I'd include that here to (and may move it in the future, if I like that
better).

