# Useful algorithms

## Quantum Bogosort

A linear sorting algorithm for arbitrary data, with odd requirements:
the many-worlds interpretation of quantum mechanics, the ability to
generate random variables via a quantum mechanical process in O(1) time,
and a mechanism with which to destroy the universe.

Provides:
* Sorting in O(N) time

Requires:
* Many-worlds interpretation of quantum mechanics
* An O(1) entangled random number generator
* A means with which to destroy the universe

See [here](https://wiki.c2.com/?QuantumBogoSort).  For some related work,
see also [here](https://xkcd.com/1185/).
